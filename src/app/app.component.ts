import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  people: Person[] = [
    {
      id: 3,
      name: 'Rutenis',
      surname: 'Turcinas',
      age: 32
    },
    {
      id: 5,
      name: 'Tomas',
      surname: 'Kazlauskas',
      age: 18
    },
    {
      id: 8,
      name: 'Mantas',
      surname: 'Petraitis',
      age: 16
    },
    {
      id: 11,
      name: 'Andrius',
      surname: 'Jonaitis',
      age: 45
    }
  ];
}
